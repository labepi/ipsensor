/**
 * Copyright (C) 2015 LabEPI
 *
 * @author Marcos Tulio de Lima Vianna
 * @author Joao Paulo de Souza Medeiros
 */

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import java.net.UnknownHostException;
import java.util.Date;

/**
* Connection class for MongoDB database.
*/
public class MongoConnection {
        
    //
    private String host;
    
    //
    private  int port;
    
    //
    private String database; 

    //
    private Mongo mongo;

    //
    private DB db;
    
    //
    private DBCollection tablePingWrapper;
    
    //
    private BasicDBObject documentPingWrapper; 

    /**
     * Constructor.
     */
    public MongoConnection() throws UnknownHostException, MongoException { 
       this.host = "localhost";
       this.port = 27017;       
       this.mongo = new Mongo(this.host, this.port); 
       this.db = mongo.getDB("test");
       this.tablePingWrapper = db.getCollection("pingWrapper");
    }

    /**
    * Inserts a capture PingWrapper.
    */   
    public void insertPingWrapper(PingWrapper p) {
        BasicDBObject document = new BasicDBObject();
        int i = 0;
        String cat = "";
        document.put("timestamp", new Date());
        document.put("url", p.getUrl());
        
        for (String s: p.getHops()) {
            i++;
            cat = String.valueOf(i);
            document.put("hop " + cat, s); 
        }
        
        this.tablePingWrapper.insert(document);
    }

    /**
    * Get all documents in collection.
    */
    public void listAllPingWrapper() {
        DBCursor cursor = tablePingWrapper.find(); 
        
        while (cursor.hasNext()) {
            System.out.println(cursor.next());
        }
    }

    /**
    * Get all catches of a specific URL.
    * @param URL to search
    */
    public void findPingWrapper(String url) {      
        DBCollection table = db.getCollection("pingWrapper");
        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("url", url);
        DBCursor cursor = table.find(searchQuery);

        while (cursor.hasNext()) {
            System.out.println(cursor.next());
        }
    }
        
    
}