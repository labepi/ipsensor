/**
 * Copyright (C) 2015 LabEPI
 *
 * @author Marcos Tulio de Lima Vianna
 * @author Joao Paulo de Souza Medeiros
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * An wrapper for the ping Unix network utility.
 */
public class PingWrapper {

    //
    private String command; 

    //
    private String url;

    //
    private int amount;

    //
    private int interval;

    //
    private int ttl;

    //
    private int wait;

    //
    private ArrayList<String> hops;

    //
    private ArrayList<String> ips;
        
    //
    private ArrayList<String> domains; 

    /**
     * Constructor.
     * @param url remote host URL
     */
    public PingWrapper(String url) {
        this.url = url;
        this.amount = 1;
        this.interval = 0;
        this.ttl = 1;
        this.wait = 5;
        this.hops = new ArrayList<String>();
        this.ips = new ArrayList<String>();
        this.domains = new ArrayList<String>();
    }

    /**
     * Print a list of the data captured with ping.
     */
    public void printList() {
        for (String l: this.hops) {
            System.out.println(l);
        }
    }

    /**
     * Execute a ping command and returns the result.
     * @param command the command to be executed
     * @return the result of running the ping command
     * @throws errors caused by failure text reading
     */
    public String ping(String command) {
        StringBuffer output = new StringBuffer();
        Process process;
        String line = "";

        try {
            process = Runtime.getRuntime().exec(command);
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String capture = output.toString();
        return capture;
    }

    public void traceroute() {
        String command;
        String output;

        for (int i = this.ttl; i < 30; i++) {
            command = "ping " + "-c " + this.amount + " -t " + i + " -W "
                     + this.wait + " " + this.url;
            output = ping(command);
            hops.add(output);

            if (stop(output, url)) {
                break;
            }
        }
    }

    /**
     * Resolve the IP address of an URL.
     * @param url the name to be resolved
     * @return the IP address from an URL
     * @throws errors caused by the failure to resolve domain names
     */
    public String findIp(String url) {
        String ip = "";

        try {
              InetAddress target = InetAddress.getByName(url);
              ip = target.getHostAddress();
        } catch(UnknownHostException e) {
            e.printStackTrace();
        }
        return ip;
    }

    /**
     * Checks if a traceroute output contains the target.
     * @param output  the output string
     * @param address the IP address of the target
     * @return true if the target is in the output string, false otherwise
     */
    public boolean stop(String output, String address) {
        String target = findIp(address);
        int count = 0;

        for (int i = 0; i < (output.length() - target.length() + 1); i++) {
            String result = output.substring(i, (i + target.length()));

            if (result.equals(target)) {
                count++;
            }
        }

        if (count == 2) {
            return true;
        }
        return false;
    }

    /**
     * @return command the command to be executed
     */
    public String getCommand() {
        return this.command;
    }

    /**
     * @return stop after sending count echo_requests packets
     */
    public int getAmount() {
        return this.amount;
    }

    /**
     * @return target computer
     */
    public String getUrl() {
        return this.url;
    }

    /**
     * @return timeout in seconds to send the next packet
     */
    public int getInterval() {
        return this.interval;
    }

    /**
     * @return time to wait for a response (in seconds)
     */
    public int getWait() {
        return this.wait;
    }

    /**
     * @return set the IP Time to Live
     */
    public int getTtl() {
        return this.ttl;
    }

    /**
     * @return list of captured data traceroute
     */
    public ArrayList<String> getHops() {
        return this.hops;
    }

    /**
     * @return list of IPs
     */
    public ArrayList<String> getIps() {
        return this.ips;
    }

    /**
     * @return list of domains
     */
    public ArrayList<String> getDomains() {
        return this.domains;
    }

    /**
     * @param command the command to be executed
     */
    public void setCommand(String command) {
        this.command = command;
    }

    /**
     * @param amount stop after sending count echo_requests packets
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * @param url target computer
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @param interval interval in seconds to send the next packet
     */
    public void setInterval(int interval) {
        this.interval = interval;
    }

    /**
     * @param wait time to wait for a response (in seconds)
     */
    public void setWait(int wait) {
        this.wait = wait;
    }

    /**
     * @param ttl set the IP Time to Live (TTL)
     */
    public void setTtl(int ttl) {
        this.ttl = ttl;
    }

    /**
     * @param hops list of captured data traceroute 
     */
    public void setHops(ArrayList<String> hops) {
        this.hops = hops;
    }
}
