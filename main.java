/**
 * Copyright (C) 2015 LabEPI
 *
 * @author Marcos Tulio de Lima Vianna
 * @author Joao Paulo de Souza Medeiros
 */

import java.net.UnknownHostException;
import java.util.Scanner;

/**
     * The main method.
     * @param args the arguments passed through command line
     */
public class main {

    public static void main(String[] args) throws UnknownHostException {
        Scanner reader = new Scanner(System.in);
        System.out.print("Target: ");
        String target = reader.next();
        PingWrapper p = new PingWrapper(target);
        MongoConnection mongo = new MongoConnection();
        p.traceroute();
        mongo.insertPingWrapper(p); 
        p.printList();
       
    }
    
}